<?php

namespace PSNDL\LoginBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PSNDLLoginBundle extends Bundle
{
    public function getParent()
    {
        //Allow override of FOSUserBundle views
        return 'FOSUserBundle';
    }
}
