<?php

namespace PSNDL\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Package
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PSNDL\MainBundle\Entity\PackageRepository")
 */
class Package
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PackageID", type="string", length=255)
     */
    private $packageID;

    /**
     * @var string
     *
     * @ORM\Column(name="Title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="DownloadURL", type="string", length=255)
     */
    private $downloadURL;

    /**
     * @var string
     *
     * @ORM\Column(name="RapName", type="string", length=255)
     */
    private $rapName;

    /**
     * @var string
     *
     * @ORM\Column(name="RapData", type="string", length=255)
     */
    private $rapData;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Author", type="string", length=255)
     */
    private $author;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Approved", type="boolean")
     */
    private $approved;

    /**
     * @var integer
     *
     * @ORM\Column(name="DownloadCount", type="integer")
     */
    private $downloadCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AddedDate", type="date")
     */
    private $addedDate;

    /**
     * @var \PSNDL\MainBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="PSNDL\MainBundle\Entity\Category", inversedBy="packages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @var \PSNDL\MainBundle\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="PSNDL\MainBundle\Entity\Region", inversedBy="package")
     * @ORM\JoinColumn(nullable=false)
     */
    private $region;

    /**
     * @var \PSNDL\MainBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="PSNDL\MainBundle\Entity\User", inversedBy="packages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set packageID
     *
     * @param string $packageID
     *
     * @return Package
     */
    public function setPackageID($packageID)
    {
        $this->packageID = $packageID;

        return $this;
    }

    /**
     * Get packageID
     *
     * @return string
     */
    public function getPackageID()
    {
        return $this->packageID;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Package
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set downloadURL
     *
     * @param string $downloadURL
     *
     * @return Package
     */
    public function setDownloadURL($downloadURL)
    {
        $this->downloadURL = $downloadURL;

        return $this;
    }

    /**
     * Get downloadURL
     *
     * @return string
     */
    public function getDownloadURL()
    {
        return $this->downloadURL;
    }

    /**
     * Set rapName
     *
     * @param string $rapName
     *
     * @return Package
     */
    public function setRapName($rapName)
    {
        $this->rapName = $rapName;

        return $this;
    }

    /**
     * Get rapName
     *
     * @return string
     */
    public function getRapName()
    {
        return $this->rapName;
    }

    /**
     * Set rapData
     *
     * @param string $rapData
     *
     * @return Package
     */
    public function setRapData($rapData)
    {
        $this->rapData = $rapData;

        return $this;
    }

    /**
     * Get rapData
     *
     * @return string
     */
    public function getRapData()
    {
        return $this->rapData;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Package
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Package
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return Package
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set downloadCount
     *
     * @param integer $downloadCount
     *
     * @return Package
     */
    public function setDownloadCount($downloadCount)
    {
        $this->downloadCount = $downloadCount;

        return $this;
    }

    /**
     * Get downloadCount
     *
     * @return integer
     */
    public function getDownloadCount()
    {
        return $this->downloadCount;
    }

    /**
     * Set addedDate
     *
     * @param \DateTime $addedDate
     *
     * @return Package
     */
    public function setAddedDate($addedDate)
    {
        $this->addedDate = $addedDate;

        return $this;
    }

    /**
     * Get addedDate
     *
     * @return \DateTime
     */
    public function getAddedDate()
    {
        return $this->addedDate;
    }

    /**
     * Set category
     *
     * @param \PSNDL\MainBundle\Entity\Category $category
     *
     * @return Package
     */
    public function setCategory(\PSNDL\MainBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \PSNDL\MainBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set region
     *
     * @param \PSNDL\MainBundle\Entity\Region $region
     *
     * @return Package
     */
    public function setRegion(\PSNDL\MainBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \PSNDL\MainBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set user
     *
     * @param \PSNDL\MainBundle\Entity\User $user
     *
     * @return Package
     */
    public function setUser(\PSNDL\MainBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PSNDL\MainBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
