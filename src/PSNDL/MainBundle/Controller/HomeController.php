<?php

namespace PSNDL\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('PSNDLMainBundle:Home:index.html.twig');
    }

    public function faqAction()
    {
        return $this->render('PSNDLMainBundle:Home:faq.html.twig');
    }

    public function helpUsAction()
    {
        return $this->render('@PSNDLMain/Home/help_us.html.twig');
    }
}
