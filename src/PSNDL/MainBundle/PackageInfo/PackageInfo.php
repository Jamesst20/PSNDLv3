<?php


namespace PSNDL\MainBundle\PackageInfo;


class PackageInfo
{
    private $licenseType;
    private $packageID;
    private $rapName;
    private $packageSize;

    private static $UNIT = array('bytes', 'KB', 'MB', 'GB');

    /*
    * License : 1 byte
    * Offset : 0xCB (usually). If license = 40, then it's a mini --> Offset 0x28B
    * License type : 01 = Network (rap required), 02 = Local (rap required), 03 = free (no rap required)
    */
    public function __construct($packageURL)
    {
        $this->licenseType = bin2hex($this->fetchData($packageURL, 0xCB, 0xCB));
        if ($this->licenseType == 40) {
            $this->licenseType = bin2hex($this->fetchData($packageURL, 0x28B, 0x28B));
        }

        $this->rapName = preg_replace('/[^a-zA-Z0-9-_]/', '', $this->fetchData($packageURL, 48, 84)) . '.RAP';
        $this->packageID = explode('_', explode('-', $this->rapName)[1])[0];

        $this->packageSize = $this->getRemoteFileSize($packageURL);
    }

    public function getLicenseType()
    {
        return $this->licenseType;
    }

    public function getLicenseName()
    {
        if ($this->licenseType == '01') {
            return 'Network license - RAP required. (0x01)';
        } else if ($this->licenseType == '02') {
            return 'Local license - RAP required. (0x02)';
        } else if ($this->licenseType == '03') {
            return 'Free license - No RAP required. (0x03)';
        }
        return 'No license or unknown';
    }

    public function getPackageID()
    {
        return $this->packageID;
    }

    public function getPackageSize()
    {
        $unitIndex = 0;
        $size = $this->packageSize;
        while ($size > 1024 && $unitIndex < count(PackageInfo::$UNIT)) {
            $size = $size / 1024;
            $unitIndex++;
        }
        return round($size, 2) . ' ' . PackageInfo::$UNIT[$unitIndex];
    }

    public function getRapName()
    {
        return $this->rapName;
    }

    public function isRapRequired()
    {
        return $this->licenseType == '01' || $this->licenseType == '02';
    }

    public function toArray()
    {
        return array(
            'PackageID' => $this->packageID,
            'RapRequired' => $this->isRapRequired(),
            'RapName' => $this->rapName,
            'LicenseType' => $this->licenseType,
            'LicenseName' => $this->getLicenseName(),
            'PackageSize' => $this->getPackageSize()
        );
    }

    private function fetchData($url, $rangeStart, $rangeEnd)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RANGE, $rangeStart . '-' . $rangeEnd);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return curl_exec($ch);
    }

    private function getRemoteFileSize($url)
    {
        static $regex = '/^Content-Length: *+\K\d++$/im';
        if (!$fp = @fopen($url, 'rb')) {
            return false;
        }
        if (isset($http_response_header) && preg_match($regex, implode("\n", $http_response_header), $matches)) {
            return (int)$matches[0];
        }
        return strlen(stream_get_contents($fp));
    }
}