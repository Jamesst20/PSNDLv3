<?php


namespace PSNDL\MainBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PackageEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('approved');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'psndl_mainbundle_packageedit';
    }

    public function getParent()
    {
        return PackageType::class;
    }
}
